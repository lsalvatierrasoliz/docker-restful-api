package com.restful.api.springsecurity.domain.repository;

import com.restful.api.springsecurity.domain.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Integer> {

    Optional<User> findOneByUsername(String username);
}
