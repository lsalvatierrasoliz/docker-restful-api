package com.restful.api.springsecurity.domain.repository;

import com.restful.api.springsecurity.domain.entity.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RoleRepository extends CrudRepository<Role, Integer> {

    Optional<Role> findOneByRoleName(String roleName);
}
