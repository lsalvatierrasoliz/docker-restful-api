package com.restful.api.springsecurity.dto;

import javax.validation.constraints.NotNull;

public class LoginDto {

    @NotNull
    private String username;

    @NotNull
    private String password;

    private String firstName;
    private String lastName;

    public LoginDto(@NotNull String username, @NotNull String password) {
        this.username = username;
        this.password = password;
    }

    public LoginDto(@NotNull String username, @NotNull String password, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Default Constructor
     */
    protected LoginDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
