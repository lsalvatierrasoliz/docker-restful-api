package com.restful.api.springsecurity.service;

import com.restful.api.springsecurity.domain.entity.Role;
import com.restful.api.springsecurity.domain.entity.User;
import com.restful.api.springsecurity.domain.repository.RoleRepository;
import com.restful.api.springsecurity.domain.repository.UserRepository;
import com.restful.api.springsecurity.security.JwtProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {


    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private UserRepository userRepository;

    private AuthenticationManager authenticationManager;

    private RoleRepository roleRepository;

    private PasswordEncoder passwordEncoder;

    private JwtProvider jwtProvider;

    @Autowired
    public UserService(UserRepository userRepository, AuthenticationManager authenticationManager, RoleRepository roleRepository, PasswordEncoder passwordEncoder,
                       JwtProvider jwtProvider) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtProvider = jwtProvider;
    }

    public Authentication signinAuth(String username, String password){
        return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    }


    public Optional<String>  signin(String username, String password){
        LOGGER.info("New user attempting to sign in");

        Optional<String> token = Optional.empty();
        Optional<User> user = userRepository.findOneByUsername(username);
        if(user.isPresent()){
            try {
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
                token = Optional.of(jwtProvider.createToken(username, user.get().getRoles()));
            } catch (AuthenticationException e){
                LOGGER.info("Log in failed for user {}", username);
            }
        }

        return token;

    }

    public List<User> getAll(){
        List<User> users = new ArrayList<>();
        userRepository.findAll().forEach( u -> users.add(u));
        return users;
    }

    public Optional<User> signup(String username, String password, String firstName, String lastName){
        if(!userRepository.findOneByUsername(username).isPresent()){
            Optional<Role> role  = roleRepository.findOneByRoleName("ROLE_CSR");

            return Optional.of(userRepository.save(
                    new User(username,
                            passwordEncoder.encode(password),
                    role.get(),
                    firstName,
                    lastName
            )));

        }

        return Optional.empty();
    }
}
