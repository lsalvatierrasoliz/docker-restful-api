Basic Example Spring Boot RestFul API for Pizza Store

# Basic Example Spring Boot RestFul API for Pizza Store
http://localhost:8080/swagger-ui.html

# Endpoints
 * Order Resource
   * Allow add new order
   * associate products to order 
   * associate pizzas to order 
   * get specific order 
 * Pizza Resource
   * create new pizza with all needed
   * associate toppings to specific pizza 
 * PizzaType Resource
   * CRUD operations over resource
 * SizePizza Resource
   * create new size of pizza
 * Topping Resource
    * CRUD operations over resource
# Requirement for deploy
* java 8 
* Postgres

# Assumptions

 #  