package com.restful.api.pizzastore.rest;

import com.restful.api.pizzastore.domain.entity.Product;
import com.restful.api.pizzastore.service.ProductService;
import com.restful.api.pizzastore.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/products")
public class ProductResource {

    @Autowired
    ProductService productService;


    @GetMapping
    public ResponseEntity<List<Product>> getAllProducts() {

        List<Product> products = productService.getAll();

        return new ResponseEntity<>(products, HttpStatus.OK);
    }


    @GetMapping("/{id}")
    public ResponseEntity<Product> getProductBy(@PathVariable("id") Long id){

        Product topping = productService.readById(id);

        if(null == topping){
            throw new ModelNotFoundException("ID NOT FOUND : " + id);
        }

        return new ResponseEntity<>(HttpStatus.OK);

    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable("id") Long id){

        Product topping = productService.readById(id);

        if(null == topping){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        productService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping
    public ResponseEntity<Product> newProduct(@RequestBody Product product){

         productService.register(product);
         return new  ResponseEntity<>(product ,HttpStatus.CREATED);

    }


    @PutMapping
    public ResponseEntity<Product> updateProduct(@RequestBody Product product){

        productService.register(product);
        return new  ResponseEntity<>(product ,HttpStatus.OK);

    }

}
