package com.restful.api.pizzastore.config;

import com.restful.api.pizzastore.interceptor.CheckStatusOrderInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Autowired
    CheckStatusOrderInterceptor checkStatusOrderInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(checkStatusOrderInterceptor)
                .addPathPatterns("/api/orders");

    }
}
