package com.restful.api.pizzastore.service.impl;

import com.restful.api.pizzastore.domain.entity.Product;
import com.restful.api.pizzastore.domain.repository.ProductRepository;
import com.restful.api.pizzastore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Product> getAll() {
        List<Product> products = new ArrayList<>();
        productRepository.findAll().forEach(products::add);

        return products;
    }

    @Override
    public Product readById(Long itemId) {
       Product product = productRepository.findById(itemId).get();
       return product;
    }

    @Override
    public Product register(Product item) {
        return productRepository.save(item);
    }

    @Override
    public Product update(Product item) {
        return productRepository.save(item);
    }

    @Override
    public void delete(Long itemId) {
        productRepository.deleteById(itemId);
    }
}
