package com.restful.api.pizzastore.domain.entity.order;


public interface OrderDetail {

    String getName();
    Integer getQuantity();
    Double getPrice();
    Order getOrder();
}
