package com.restful.api.pizzastore.service;

import com.restful.api.pizzastore.domain.entity.pizza.Topping;

public interface ToppingService extends CrudService<Topping> {

}
