package com.restful.api.pizzastore.domain.entity.order;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.restful.api.pizzastore.domain.entity.Product;

import javax.persistence.*;

@Entity
public class OrderDetailProduct implements OrderDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_detail_product_id")
    private Long orderDetailProductId;

    private Integer quantity;

    private Double price;

    private String name;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    private Order order;

    public Long getOrderDetailProductId() {
        return orderDetailProductId;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Integer getQuantity() {
        return quantity;
    }

    @Override
    public Double getPrice() {
        return price;
    }

    @Override
    public Order getOrder() {
        return order;
    }

    public void setOrderDetailProductId(Long orderDetailProductId) {
        this.orderDetailProductId = orderDetailProductId;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public void setName(String name) {
        this.name = name;
    }
}
