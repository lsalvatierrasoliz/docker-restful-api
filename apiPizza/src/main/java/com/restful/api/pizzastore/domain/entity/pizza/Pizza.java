package com.restful.api.pizzastore.domain.entity.pizza;


import com.restful.api.pizzastore.domain.entity.pizza.cons.CheeseType;
import com.restful.api.pizzastore.domain.entity.pizza.cons.CrustType;
import com.restful.api.pizzastore.domain.entity.pizza.cons.SauceType;

import javax.persistence.*;

@Entity
public class Pizza {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long pizzaId;

    @ManyToOne
    @JoinColumn(name = "size_id", nullable = false, foreignKey = @ForeignKey(name = "fk_pizza_size"))
    private Size size;

    @ManyToOne
    @JoinColumn(name = "pizza_type_id", nullable = false, foreignKey = @ForeignKey(name = "fk_pizza_pizza_type"))
    private PizzaType pizzaType;

    @Enumerated(EnumType.STRING)
    private CheeseType cheeseType;

    @Enumerated(EnumType.STRING)
    private SauceType sauceType;

    @Enumerated(EnumType.STRING)
    private CrustType crustType;

    public Long getPizzaId() {
        return pizzaId;
    }

    public void setPizzaId(Long pizzaId) {
        this.pizzaId = pizzaId;
    }


    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public PizzaType getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(PizzaType pizzaType) {
        this.pizzaType = pizzaType;
    }

    public CheeseType getCheeseType() {
        return cheeseType;
    }

    public void setCheeseType(CheeseType cheeseType) {
        this.cheeseType = cheeseType;
    }


    public SauceType getSauceType() {
        return sauceType;
    }

    public void setSauceType(SauceType sauceType) {
        this.sauceType = sauceType;
    }

    public CrustType getCrustType() {
        return crustType;
    }

    public void setCrustType(CrustType crustType) {
        this.crustType = crustType;
    }
}
