package com.restful.api.pizzastore.domain.entity.order.cons;

public enum OrderStatus {
    PENDING, CONFIRMED, READY, DELIVERED
}
