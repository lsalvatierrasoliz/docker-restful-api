package com.restful.api.pizzastore.interceptor;

import com.restful.api.pizzastore.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class CheckStatusOrderInterceptor extends HandlerInterceptorAdapter {


    @Autowired
    OrderService orderService;

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {

        if(RequestMethod.PUT.name().equals(request.getMethod())){
            System.out.println("Checking all confirmed orders and send it to prepare it");
            //orderService.checkConfirmedOrders();
        }

    }

}
