package com.restful.api.pizzastore.domain.repository;

import com.restful.api.pizzastore.domain.entity.order.OrderDetailProduct;
import org.springframework.data.repository.CrudRepository;

public interface OrderDetailProductRepository extends CrudRepository<OrderDetailProduct, Long> {
}
