package com.restful.api.pizzastore.domain.entity.order;

import com.restful.api.pizzastore.domain.entity.order.cons.OrderStatus;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    private Long orderId;

    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @OneToMany(mappedBy = "order", cascade = { CascadeType.ALL, CascadeType.MERGE,
            CascadeType.REMOVE }, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<OrderDetailProduct> orderDetailProduct;

    @OneToMany(mappedBy = "order", cascade = { CascadeType.ALL, CascadeType.MERGE,
            CascadeType.REMOVE }, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<OrderDetailPizza> orderDetailPizza;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Set<OrderDetailProduct> getOrderDetailProduct() {
        return orderDetailProduct;
    }

    public void setOrderDetailProduct(Set<OrderDetailProduct> orderDetailProduct) {
        this.orderDetailProduct = orderDetailProduct;
    }

    public Set<OrderDetailPizza> getOrderDetailPizza() {
        return orderDetailPizza;
    }

    public void setOrderDetailPizza(Set<OrderDetailPizza> orderDetailPizza) {
        this.orderDetailPizza = orderDetailPizza;
    }
}
