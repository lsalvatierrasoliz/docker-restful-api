package com.restful.api.pizzastore.helper;

import com.restful.api.pizzastore.domain.entity.pizza.Pizza;
import org.springframework.stereotype.Component;

@Component
public class PizzaShop {

    public Boolean takeOrderPizza(Pizza pizza){

        prepare(pizza);
        bake(pizza);
        cut(pizza);
        box(pizza);

        return Boolean.TRUE;
    }

    void prepare(Pizza pizza) {
        System.out.println("Prepare " + pizza.getPizzaType().getName());
        System.out.println("Tossing dough...");
        System.out.println("Adding sauce...");
        System.out.println("Adding toppings: ");
    }

    void bake(Pizza pizza) {
        System.out.println("Bake for 25 minutes at 350");
    }

    void cut(Pizza pizza) {
        System.out.println("Cut the pizza into diagonal slices");
    }

    void box(Pizza pizza) {
        System.out.println("Place pizza in official PizzaStore box");
    }
}
