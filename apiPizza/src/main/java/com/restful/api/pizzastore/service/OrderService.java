package com.restful.api.pizzastore.service;

import com.restful.api.pizzastore.domain.entity.order.Order;
import com.restful.api.pizzastore.domain.entity.order.cons.OrderStatus;
import com.restful.api.pizzastore.dto.AddOrderItemDTO;

public interface OrderService {

    Order getOrder(Long orderId);
    Order createOrder();
    Order addProductToOrder(Long orderId, AddOrderItemDTO orderProductDTO);
    Order addPizzaToOrder(Long orderId, AddOrderItemDTO orderProductDTO);
    void updateOrderStatus(Long orderId, OrderStatus orderStatus);
    void checkConfirmedOrders();
}
