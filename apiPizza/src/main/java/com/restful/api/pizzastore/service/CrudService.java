package com.restful.api.pizzastore.service;

import java.util.List;

public interface CrudService<T> {

    List<T> getAll();
    T readById(Long itemId);
    T register(T item);
    T update(T item);
    void delete(Long itemId);

}
