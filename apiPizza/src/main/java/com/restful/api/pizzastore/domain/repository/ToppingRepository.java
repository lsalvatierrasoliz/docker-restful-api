package com.restful.api.pizzastore.domain.repository;

import com.restful.api.pizzastore.domain.entity.pizza.Topping;
import org.springframework.data.repository.CrudRepository;

public interface ToppingRepository extends CrudRepository<Topping, Long> {
}
