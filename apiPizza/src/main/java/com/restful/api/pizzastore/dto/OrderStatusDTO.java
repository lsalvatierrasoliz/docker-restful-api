package com.restful.api.pizzastore.dto;

import com.restful.api.pizzastore.domain.entity.order.cons.OrderStatus;

public class OrderStatusDTO {

    private Long orderId;
    private OrderStatus orderStatus;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }
}
