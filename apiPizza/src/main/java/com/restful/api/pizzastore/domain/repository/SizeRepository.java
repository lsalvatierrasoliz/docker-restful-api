package com.restful.api.pizzastore.domain.repository;

import com.restful.api.pizzastore.domain.entity.pizza.Size;
import com.restful.api.pizzastore.domain.entity.pizza.cons.PizzaSizeType;
import org.springframework.data.repository.CrudRepository;

public interface SizeRepository extends CrudRepository<Size, Long> {

    Size findByPizzaSizeType(PizzaSizeType pizzaSize);

}
