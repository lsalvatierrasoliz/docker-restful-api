package com.restful.api.pizzastore.domain.entity;

import java.math.BigDecimal;

public interface Priceable {
    BigDecimal getPrice();
}
