package com.restful.api.pizzastore.service;

import com.restful.api.pizzastore.domain.entity.pizza.Pizza;
import com.restful.api.pizzastore.dto.PizzaToppingDTO;

public interface PizzaService {
    Pizza registrarPizza(PizzaToppingDTO pizzaToppingDTO);
    Pizza addTopping(Long pizzaId, Long toppingId);

}
