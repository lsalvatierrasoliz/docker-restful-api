package com.restful.api.pizzastore.dto;

import com.restful.api.pizzastore.domain.entity.pizza.Topping;
import com.restful.api.pizzastore.domain.entity.pizza.cons.CheeseType;
import com.restful.api.pizzastore.domain.entity.pizza.cons.CrustType;
import com.restful.api.pizzastore.domain.entity.pizza.cons.PizzaSizeType;
import com.restful.api.pizzastore.domain.entity.pizza.cons.SauceType;

import java.util.List;

public class PizzaToppingDTO {

    private CheeseType cheeseType;
    private SauceType sauceType;
    private CrustType crustType;

    private Long pizzaTypeId;
    private PizzaSizeType pizzaSizeType;

    private List<Topping> toppingList;

    public List<Topping> getToppingList() {
        return toppingList;
    }

    public void setToppingList(List<Topping> toppingList) {
        this.toppingList = toppingList;
    }

    public CheeseType getCheeseType() {
        return cheeseType;
    }

    public void setCheeseType(CheeseType cheeseType) {
        this.cheeseType = cheeseType;
    }

    public SauceType getSauceType() {
        return sauceType;
    }

    public void setSauceType(SauceType sauceType) {
        this.sauceType = sauceType;
    }

    public CrustType getCrustType() {
        return crustType;
    }

    public void setCrustType(CrustType crustType) {
        this.crustType = crustType;
    }

    public Long getPizzaTypeId() {
        return pizzaTypeId;
    }

    public void setPizzaTypeId(Long pizzaTypeId) {
        this.pizzaTypeId = pizzaTypeId;
    }

    public PizzaSizeType getPizzaSizeType() {
        return pizzaSizeType;
    }

    public void setPizzaSizeType(PizzaSizeType pizzaSizeType) {
        this.pizzaSizeType = pizzaSizeType;
    }
}
