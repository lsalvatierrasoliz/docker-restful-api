package com.restful.api.pizzastore.dto;

public class AddToppingDTO {

    private Long toppingId;

    public Long getToppingId() {
        return toppingId;
    }

    public void setToppingId(Long toppingId) {
        this.toppingId = toppingId;
    }
}
