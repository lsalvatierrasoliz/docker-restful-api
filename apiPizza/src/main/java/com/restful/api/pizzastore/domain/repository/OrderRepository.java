package com.restful.api.pizzastore.domain.repository;

import com.restful.api.pizzastore.domain.entity.order.Order;
import com.restful.api.pizzastore.domain.entity.order.cons.OrderStatus;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Long> {

    List<Order> findByOrderStatus(OrderStatus orderStatus);
}
