package com.restful.api.pizzastore.service;

import com.restful.api.pizzastore.domain.entity.Product;

public interface ProductService extends CrudService<Product> {
}
