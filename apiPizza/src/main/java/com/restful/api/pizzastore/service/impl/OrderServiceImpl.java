package com.restful.api.pizzastore.service.impl;

import com.restful.api.pizzastore.domain.entity.Product;
import com.restful.api.pizzastore.domain.entity.order.Order;
import com.restful.api.pizzastore.domain.entity.order.OrderDetailPizza;
import com.restful.api.pizzastore.domain.entity.order.OrderDetailProduct;
import com.restful.api.pizzastore.domain.entity.order.cons.OrderStatus;
import com.restful.api.pizzastore.domain.entity.pizza.Pizza;
import com.restful.api.pizzastore.domain.repository.*;
import com.restful.api.pizzastore.dto.AddOrderItemDTO;
import com.restful.api.pizzastore.helper.PizzaShop;
import com.restful.api.pizzastore.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderDetailProductRepository orderDetailProductRepository;

    @Autowired
    OrderDetailPizzaRepository orderDetailPizzaRepository;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    PizzaRepository pizzaRepository;

    @Override
    public Order getOrder(Long orderId) {
        return orderRepository.findById(orderId).get();
    }

    @Override
    public Order createOrder() {
        Order order = new Order();
        order.setOrderStatus(OrderStatus.PENDING);

        return orderRepository.save(order);
    }

    @Override
    public Order addProductToOrder(Long orderId, AddOrderItemDTO orderProductDTO) {


        Order order = orderRepository.findById(orderId).get();
        Product product = productRepository.findById(orderProductDTO.getItemId()).get();

        OrderDetailProduct orderDetailProduct = new OrderDetailProduct();
        orderDetailProduct.setName(product.getName());
        orderDetailProduct.setQuantity(orderProductDTO.getQuantity());
        orderDetailProduct.setPrice(product.getPrice().doubleValue());
        orderDetailProduct.setProduct(product);
        orderDetailProduct.setOrder(order);


        orderDetailProductRepository.save(orderDetailProduct);

        return order;


    }

    @Override
    public Order addPizzaToOrder(Long orderId, AddOrderItemDTO orderProductDTO) {

        Order order = orderRepository.findById(orderId).get();
        Pizza pizza = pizzaRepository.findById(orderProductDTO.getItemId()).get();

        OrderDetailPizza orderDetailPizza = new OrderDetailPizza();
        orderDetailPizza.setName(pizza.getPizzaType().getName());
        orderDetailPizza.setQuantity(orderProductDTO.getQuantity());
        orderDetailPizza.setPrice(pizza.getSize().getPrice().doubleValue());
        orderDetailPizza.setPizza(pizza);
        orderDetailPizza.setOrder(order);

        orderDetailPizzaRepository.save(orderDetailPizza);

        return order;

    }

    @Override
    public void updateOrderStatus(Long orderId, OrderStatus orderStatus) {
       Order orderUpdate =  orderRepository.findById(orderId).get();
       orderUpdate.setOrderStatus(orderStatus);

       orderRepository.save(orderUpdate);

    }

    @Autowired
    PizzaShop pizzaShop;

    /**
     * Simulate pass confirmed orders for preparing and then change order status to "Ready" to Deliver
     * TODO : OrderDetailPizza is lazy for oder, so error is getting up ....
     */
    @Override
    public void checkConfirmedOrders() {

        List<Order> confirmedOrders = orderRepository.findByOrderStatus(OrderStatus.CONFIRMED);
        confirmedOrders.forEach( order -> {
            order.getOrderDetailPizza().forEach(pizzaDetail ->
                    pizzaShop.takeOrderPizza(pizzaDetail.getPizza()));

                    order.setOrderStatus(OrderStatus.READY);
                    orderRepository.save(order);
                }
        );
    }
}
