package com.restful.api.pizzastore.domain.repository;

import com.restful.api.pizzastore.domain.entity.pizza.PizzaTopping;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PizzaToppingRepository extends CrudRepository<PizzaTopping, Long> {

    @Modifying
    @Query(value = "INSERT INTO pizza_topping(pizza_id, topping_id) VALUES(:pizzaId, :toppingId)", nativeQuery = true)
    Integer savePizzaTopping(@Param("pizzaId") Long pizzaId, @Param("toppingId") Long toppingId);

}
