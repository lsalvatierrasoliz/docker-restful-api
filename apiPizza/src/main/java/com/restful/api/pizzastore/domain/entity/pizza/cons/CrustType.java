package com.restful.api.pizzastore.domain.entity.pizza.cons;

public enum CrustType {
     THIN,
     THICK,
     FILLED_CHESSE,
}
