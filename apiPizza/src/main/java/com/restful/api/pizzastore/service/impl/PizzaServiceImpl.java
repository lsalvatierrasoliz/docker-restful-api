package com.restful.api.pizzastore.service.impl;

import com.restful.api.pizzastore.domain.entity.pizza.Pizza;
import com.restful.api.pizzastore.domain.entity.pizza.PizzaType;
import com.restful.api.pizzastore.domain.entity.pizza.Size;
import com.restful.api.pizzastore.domain.repository.*;
import com.restful.api.pizzastore.dto.PizzaToppingDTO;
import com.restful.api.pizzastore.service.PizzaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class PizzaServiceImpl implements PizzaService {

    @Autowired
    PizzaRepository pizzaRepository;

    @Autowired
    PizzaToppingRepository pizzaToppingRepository;

    @Autowired
    PizzaTypeRepository pizzaTypeRepository;

    @Autowired
    SizeRepository sizeRepository;

    @Autowired
    ToppingRepository toppingRepository;

    @Transactional
    @Override
    public Pizza registrarPizza(PizzaToppingDTO pizzaToppingDTO) {

          PizzaType pizzaType = pizzaTypeRepository.findById(pizzaToppingDTO.getPizzaTypeId()).get();
          Size size =  sizeRepository.findByPizzaSizeType(pizzaToppingDTO.getPizzaSizeType());


          Pizza pizza = new Pizza();
          pizza.setCheeseType(pizzaToppingDTO.getCheeseType());
          pizza.setCrustType(pizzaToppingDTO.getCrustType());
          pizza.setSauceType(pizzaToppingDTO.getSauceType());
          pizza.setPizzaType(pizzaType);
          pizza.setSize(size);

          pizzaRepository.save(pizza);

          pizzaToppingDTO.getToppingList().forEach( topping -> {
              pizzaToppingRepository.savePizzaTopping(pizza.getPizzaId(), topping.getToppingId());
          });

          return pizza;

    }

    @Transactional
    @Override
    public Pizza addTopping(Long pizzaId, Long toppingId) {
        pizzaToppingRepository.savePizzaTopping(pizzaId, toppingId);

        return pizzaRepository.findById(pizzaId).get();
    }
}
