package com.restful.api.pizzastore.domain.repository;

import com.restful.api.pizzastore.domain.entity.pizza.Pizza;
import org.springframework.data.repository.CrudRepository;

public interface PizzaRepository extends CrudRepository<Pizza, Long> {
}
