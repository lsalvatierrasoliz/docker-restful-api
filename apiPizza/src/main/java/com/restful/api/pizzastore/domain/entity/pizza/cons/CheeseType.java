package com.restful.api.pizzastore.domain.entity.pizza.cons;

public enum CheeseType {

    CHEDDAR,
    PROVOLONE,
    MOZZARELLA
}
