package com.restful.api.pizzastore.rest;

import com.restful.api.pizzastore.domain.entity.pizza.PizzaType;
import com.restful.api.pizzastore.service.PizzaTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/pizzaTypes")
public class PizzaTypeResource {

    @Autowired
    PizzaTypeService pizzaTypeService;


    @GetMapping
    public ResponseEntity<List<PizzaType>> getAllPizzaTypes(){

        List<PizzaType> pizzaTypes = pizzaTypeService.getAll();

        return  new ResponseEntity<List<PizzaType>>(pizzaTypes, HttpStatus.OK);

    }

    @GetMapping("/{id}")
    public ResponseEntity<PizzaType> getPizzaTypeById(@PathVariable("id") Long pizzaTypeId){

        PizzaType pizzaType = pizzaTypeService.readById(pizzaTypeId);

        return  new ResponseEntity<>(pizzaType, HttpStatus.OK);

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePizzaType(@PathVariable Long id){
        pizzaTypeService.delete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping
    public ResponseEntity<PizzaType> newPizzaType(@RequestBody PizzaType pizzaType){

        pizzaType = pizzaTypeService.register(pizzaType);
        return new ResponseEntity<>(pizzaType, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<PizzaType> updateTopping(@RequestBody PizzaType pizzaType){

        pizzaTypeService.update(pizzaType);
        return new  ResponseEntity<>(pizzaType ,HttpStatus.OK);

    }
}
