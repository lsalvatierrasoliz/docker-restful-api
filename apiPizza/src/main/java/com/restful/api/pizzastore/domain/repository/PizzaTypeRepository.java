package com.restful.api.pizzastore.domain.repository;

import com.restful.api.pizzastore.domain.entity.pizza.PizzaType;
import org.springframework.data.repository.CrudRepository;

public interface PizzaTypeRepository extends CrudRepository<PizzaType, Long> {
}
