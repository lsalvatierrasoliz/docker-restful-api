package com.restful.api.pizzastore.domain.repository;

import com.restful.api.pizzastore.domain.entity.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {
}
