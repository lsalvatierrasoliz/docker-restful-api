package com.restful.api.pizzastore.service.impl;

import com.restful.api.pizzastore.domain.entity.pizza.PizzaType;
import com.restful.api.pizzastore.domain.repository.PizzaTypeRepository;
import com.restful.api.pizzastore.service.PizzaTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PizzaTypeServiceImpl implements PizzaTypeService {

    @Autowired
    PizzaTypeRepository pizzaTypeRepository;

    @Override
    public List<PizzaType> getAll() {

        List<PizzaType> pizzaTypes = new ArrayList<>();
        pizzaTypeRepository.findAll().forEach( pt -> pizzaTypes.add(pt));

        return pizzaTypes;
    }

    @Override
    public PizzaType readById(Long itemId) {
        return pizzaTypeRepository.findById(itemId).get();
    }

    @Override
    public PizzaType register(PizzaType item) {
        return pizzaTypeRepository.save(item);
    }

    @Override
    public PizzaType update(PizzaType item) {
                                            return pizzaTypeRepository.save(item);
    }

    @Override
    public void delete(Long itemId) {
        pizzaTypeRepository.deleteById(itemId);
    }
}
