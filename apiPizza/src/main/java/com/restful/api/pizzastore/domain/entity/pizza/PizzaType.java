package com.restful.api.pizzastore.domain.entity.pizza;

import com.restful.api.pizzastore.domain.entity.Priceable;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Entity
public class PizzaType implements Priceable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "pizza_type_id")
    private Long pizzaTypeId;

    @Size(min = 3, message = "Name pizza must be at least 3 characters")
    @Column(name = "pizzaType_name", nullable = false, length = 30)
    private String name;

    @Column(name = "pizzaType_description", columnDefinition = "text", length = 150)
    private String description;

    @Column(name="pizzaType_price")
    private BigDecimal price;

    public Long getPizzaTypeId() {
        return pizzaTypeId;
    }

    public void setPizzaTypeId(Long pizzaTypeId) {
        this.pizzaTypeId = pizzaTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price){
        this.price = price;
    }
}
