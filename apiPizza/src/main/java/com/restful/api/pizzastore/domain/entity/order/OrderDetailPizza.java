package com.restful.api.pizzastore.domain.entity.order;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.restful.api.pizzastore.domain.entity.pizza.Pizza;

import javax.persistence.*;

@Entity
public class OrderDetailPizza implements OrderDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_detail_pizza_id")
    private Long orderDetailPizzaId;

    private Integer quantity;

    private Double price;

    private String name;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "pizza_id", nullable = false)
    private Pizza pizza;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    private Order order;

    public Long getOrderDetailPizzaId() {
        return orderDetailPizzaId;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Integer getQuantity() {
        return quantity;
    }

    @Override
    public Double getPrice() {
        return price;
    }

    @Override
    public Order getOrder() {
        return order;
    }

    public void setOrderDetailPizzaId(Long orderDetailPizzaId) {
        this.orderDetailPizzaId = orderDetailPizzaId;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public void setName(String name) {
        this.name = name;
    }
}



