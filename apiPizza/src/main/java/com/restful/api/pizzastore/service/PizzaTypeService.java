package com.restful.api.pizzastore.service;

import com.restful.api.pizzastore.domain.entity.pizza.PizzaType;

public interface PizzaTypeService extends CrudService<PizzaType> {

}
