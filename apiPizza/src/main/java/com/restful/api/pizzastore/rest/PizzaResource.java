package com.restful.api.pizzastore.rest;

import com.restful.api.pizzastore.domain.entity.pizza.Pizza;
import com.restful.api.pizzastore.domain.entity.pizza.Topping;
import com.restful.api.pizzastore.dto.AddToppingDTO;
import com.restful.api.pizzastore.dto.PizzaToppingDTO;
import com.restful.api.pizzastore.service.PizzaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/api/pizzas")
public class PizzaResource {

    @Autowired
    PizzaService pizzaService;


    @PostMapping
    public ResponseEntity<Pizza> newPizza(@Valid @RequestBody PizzaToppingDTO pizzaModel) {

        Pizza pizza = pizzaService.registrarPizza(pizzaModel);
        return new ResponseEntity<>(pizza, HttpStatus.CREATED);
    }

    @PostMapping("/{idPizza}/toppings")
    public ResponseEntity<Pizza> addTopping(@PathVariable("idPizza") Long idPizza, @RequestBody AddToppingDTO topping){

        Pizza pizza = pizzaService.addTopping(idPizza, topping.getToppingId());
        return new ResponseEntity<>(pizza, HttpStatus.CREATED);
    }
}
