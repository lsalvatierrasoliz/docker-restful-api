package com.restful.api.pizzastore.domain.entity.pizza;

import com.restful.api.pizzastore.domain.entity.Priceable;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
public class Topping implements Priceable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="topping_id")
    private Long toppingId;

    @Column(name = "topping_description", nullable = false)
    private String description;

    @Column(name = "topping_price")
    private BigDecimal price;

    public Long getToppingId() {
        return toppingId;
    }

    public void setToppingId(Long toppingId) {
        this.toppingId = toppingId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
