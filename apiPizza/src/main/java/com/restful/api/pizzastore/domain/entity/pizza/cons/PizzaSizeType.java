package com.restful.api.pizzastore.domain.entity.pizza.cons;


public enum PizzaSizeType {
    PERSONAL,
    SMALL,
    MEDIUM,
    LARGE

}
