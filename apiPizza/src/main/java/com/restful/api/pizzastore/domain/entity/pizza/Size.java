package com.restful.api.pizzastore.domain.entity.pizza;

import com.restful.api.pizzastore.domain.entity.Priceable;
import com.restful.api.pizzastore.domain.entity.pizza.cons.PizzaSizeType;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "Pizza_Size")
public class Size implements Priceable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name= "size_id")
    private Long sizeId;

    @Enumerated(EnumType.STRING)
    private PizzaSizeType pizzaSizeType;

    @Column(name = "size_description")
    private String description;

    @Column(name = "slice_number")
    private Integer numberSlice;

    @Column(name = "size_price")
    private BigDecimal price;

    public Long getSizeId() {
        return sizeId;
    }

    public void setSizeId(Long sizeId) {
        this.sizeId = sizeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNumberSlice() {
        return numberSlice;
    }

    public void setNumberSlice(Integer numberSlice) {
        this.numberSlice = numberSlice;
    }

    public PizzaSizeType getPizzaSize() {
        return pizzaSizeType;
    }

    public void setPizzaSize(PizzaSizeType pizzaSize) {
        this.pizzaSizeType = pizzaSize;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
