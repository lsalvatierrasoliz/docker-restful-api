package com.restful.api.pizzastore.rest;

import com.restful.api.pizzastore.domain.entity.order.Order;
import com.restful.api.pizzastore.dto.AddOrderItemDTO;
import com.restful.api.pizzastore.dto.OrderStatusDTO;
import com.restful.api.pizzastore.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/orders")
public class OrderResource {


    @Autowired
    OrderService orderService;

    @PostMapping
    private ResponseEntity<Order>  createOrder() {

        Order order = orderService.createOrder();

        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }

    @PostMapping("/{idOrder}/products")
    private ResponseEntity<Order> addProductToOrder(@PathVariable("idOrder") Long idOrder, @RequestBody AddOrderItemDTO orderProductDTO){

        Order order = orderService.addProductToOrder(idOrder, orderProductDTO);

        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }

    @PostMapping("/{idOrder}/pizzas")
    private ResponseEntity<Order> addPizzaToOrder(@PathVariable("idOrder") Long idOrder, @RequestBody AddOrderItemDTO orderProductDTO){

        Order order = orderService.addPizzaToOrder(idOrder, orderProductDTO);

        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }

    @PutMapping
    private ResponseEntity<Order> updateOrderStatus(@RequestBody OrderStatusDTO orderStatusDTO) {

        orderService.updateOrderStatus(orderStatusDTO.getOrderId(), orderStatusDTO.getOrderStatus());

        return new ResponseEntity<>(new Order(), HttpStatus.OK);
    }

    @GetMapping("/{orderId}")
    private ResponseEntity<Order> getOrderById(@PathVariable("orderId") Long orderId) {

        Order order = orderService.getOrder(orderId);

        return new ResponseEntity<>(order, HttpStatus.OK);
    }
}
