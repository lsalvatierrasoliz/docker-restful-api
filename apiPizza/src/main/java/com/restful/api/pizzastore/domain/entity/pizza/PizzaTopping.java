package com.restful.api.pizzastore.domain.entity.pizza;

import javax.persistence.*;

@Entity
public class PizzaTopping {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long pizzaToppingId;

    @ManyToOne
    @JoinColumn(name = "topping_id", nullable = false)
    private Topping topping;

    @ManyToOne
    @JoinColumn(name = "pizza_id", nullable = false)
    private Pizza pizza;

    public Long getPizzaToppingId() {
        return pizzaToppingId;
    }

    public void setPizzaToppingId(Long pizzaToppingId) {
        this.pizzaToppingId = pizzaToppingId;
    }

    public Topping getTopping() {
        return topping;
    }

    public void setTopping(Topping topping) {
        this.topping = topping;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }
}
