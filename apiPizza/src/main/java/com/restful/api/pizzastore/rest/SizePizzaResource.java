package com.restful.api.pizzastore.rest;


import com.restful.api.pizzastore.domain.entity.pizza.Size;
import com.restful.api.pizzastore.domain.repository.SizeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/api/sizes")
public class SizePizzaResource {

    @Autowired
    SizeRepository sizeRepository;


    @PostMapping
    public ResponseEntity<Size> newSize(@RequestBody Size size){
        sizeRepository.save(size);
        return new ResponseEntity<>(size ,HttpStatus.CREATED);
    }
}
