package com.restful.api.pizzastore.rest;



import com.restful.api.pizzastore.domain.entity.pizza.Topping;
import com.restful.api.pizzastore.service.ToppingService;
import com.restful.api.pizzastore.exception.ModelNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/toppings")
public class ToppingResource {

    @Autowired
    ToppingService toppingService;


    @GetMapping
    public ResponseEntity<List<Topping>> getAllToppings() {

        List<Topping> toppings = toppingService.getAll();

        return new ResponseEntity<List<Topping>>(toppings, HttpStatus.OK);
    }


    @GetMapping("/{id}")
    public ResponseEntity<Topping> getToppingBy(@PathVariable("id") Long id){

        Topping topping = toppingService.readById(id);

        if(null == topping){
            throw new ModelNotFoundException("ID NOT FOUND : " + id);
        }

        return new ResponseEntity<>(HttpStatus.OK);

    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTopping(@PathVariable("id") Long id){

        Topping topping = toppingService.readById(id);

        if(null == topping){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        toppingService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping
    public ResponseEntity<Topping> newTopping(@RequestBody Topping topping){

         toppingService.register(topping);
         return new  ResponseEntity<>(topping ,HttpStatus.CREATED);

    }


    @PutMapping
    public ResponseEntity<Topping> updateTopping(@RequestBody Topping topping){

        toppingService.register(topping);
        return new  ResponseEntity<>(topping ,HttpStatus.OK);

    }

}
