package com.restful.api.pizzastore.service.impl;

import com.restful.api.pizzastore.domain.entity.pizza.Topping;
import com.restful.api.pizzastore.domain.repository.ToppingRepository;
import com.restful.api.pizzastore.service.ToppingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ToppingServiceImpl implements ToppingService {

    @Autowired
    ToppingRepository toppingRepository;


    @Override
    public List<Topping> getAll() {
        List<Topping> toppings = new ArrayList<>();
        toppingRepository.findAll().forEach( pt -> toppings.add(pt));

        return toppings;
    }

    @Override
    public Topping readById(Long itemId) {
        return toppingRepository.findById(itemId).get();
    }

    @Override
    public Topping register(Topping item) {
        return toppingRepository.save(item);
    }

    @Override
    public Topping update(Topping item) {
        return toppingRepository.save(item);
    }

    @Override
    public void delete(Long itemId) {
        toppingRepository.deleteById(itemId);
    }
}
