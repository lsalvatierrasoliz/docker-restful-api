-- Table: public.security_role

-- DROP TABLE public.security_role;

CREATE TABLE public.security_role
(
    id serial NOT NULL,
    description character varying(255) COLLATE pg_catalog."default",
    role_name character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT security_role_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.security_role
    OWNER to postgres;



-- Table: public.security_user

-- DROP TABLE public.security_user;

CREATE TABLE public.security_user
(
    id serial NOT NULL,
    first_name character varying(255) COLLATE pg_catalog."default",
    last_name character varying(255) COLLATE pg_catalog."default",
    password character varying(255) COLLATE pg_catalog."default",
    username character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT security_user_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.security_user
    OWNER to postgres;



-- Table: public.user_role

-- DROP TABLE public.user_role;

CREATE TABLE public.user_role
(
    user_id integer NOT NULL,
    role_id integer NOT NULL,
    CONSTRAINT fkag2tat8o2o7yvedewuh0tosbq FOREIGN KEY (user_id)
        REFERENCES public.security_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fkaovu9xgrvfngaab129ho0e6s1 FOREIGN KEY (role_id)
        REFERENCES public.security_role (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.user_role
    OWNER to postgres;