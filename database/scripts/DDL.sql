-- Table: public.topping

-- DROP TABLE public.topping;

CREATE TABLE public.topping
(
    topping_id serial NOT NULL,
    topping_description character varying(255) COLLATE pg_catalog."default" NOT NULL,
    topping_price numeric(19,2),
    CONSTRAINT topping_pkey PRIMARY KEY (topping_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.topping
    OWNER to postgres;
	
	
-- Table: public.product

-- DROP TABLE public.product;

CREATE TABLE public.product
(
    product_id serial NOT NULL,
    product_description character varying(255) COLLATE pg_catalog."default",
    product_name character varying(255) COLLATE pg_catalog."default",
    product_price numeric(19,2),
    CONSTRAINT product_pkey PRIMARY KEY (product_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.product
    OWNER to postgres;

-- Table: public.pizza_type

-- DROP TABLE public.pizza_type;

CREATE TABLE public.pizza_type
(
    pizza_type_id serial NOT NULL,
    pizza_type_description text COLLATE pg_catalog."default",
    pizza_type_name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    pizza_type_price numeric(19,2),
    CONSTRAINT pizza_type_pkey PRIMARY KEY (pizza_type_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.pizza_type
    OWNER to postgres;	
	
	
-- Table: public.pizza_size

-- DROP TABLE public.pizza_size;

CREATE TABLE public.pizza_size
(
    size_id serial NOT NULL,
    size_description character varying(255) COLLATE pg_catalog."default",
    slice_number integer,
    pizza_size_type character varying(255) COLLATE pg_catalog."default",
    size_price numeric(19,2),
    CONSTRAINT pizza_size_pkey PRIMARY KEY (size_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.pizza_size
    OWNER to postgres;


------------------------------------****************-----------------------


	
	-- Table: public.pizza

-- DROP TABLE public.pizza;

CREATE TABLE public.pizza
(
    pizza_id serial NOT NULL,
    cheese_type character varying(255) COLLATE pg_catalog."default",
    crust_type character varying(255) COLLATE pg_catalog."default",
    sauce_type character varying(255) COLLATE pg_catalog."default",
    pizza_type_id integer NOT NULL,
    size_id integer NOT NULL,
    CONSTRAINT pizza_pkey PRIMARY KEY (pizza_id),
    CONSTRAINT fk_pizza_pizza_type FOREIGN KEY (pizza_type_id)
        REFERENCES public.pizza_type (pizza_type_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_pizza_size FOREIGN KEY (size_id)
        REFERENCES public.pizza_size (size_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.pizza
    OWNER to postgres;
	
	
	
-- Table: public.pizza_topping

-- DROP TABLE public.pizza_topping;

CREATE TABLE public.pizza_topping
(
    pizza_topping_id serial NOT NULL,
    pizza_id integer NOT NULL,
    topping_id integer NOT NULL,
    CONSTRAINT pizza_topping_pkey PRIMARY KEY (pizza_topping_id),
    CONSTRAINT fkdrsqb61c24ogsdowf37v9yu5 FOREIGN KEY (pizza_id)
        REFERENCES public.pizza (pizza_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fknhdfrce56x4abcpco6b32hh95 FOREIGN KEY (topping_id)
        REFERENCES public.topping (topping_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.pizza_topping
    OWNER to postgres;	
	

-----***************---------------------

	
-- Table: public.orders

-- DROP TABLE public.orders;

CREATE TABLE public.orders
(
    order_id serial NOT NULL,
    order_status character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT orders_pkey PRIMARY KEY (order_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.orders
    OWNER to postgres;
	


-- Table: public.order_detail_pizza

-- DROP TABLE public.order_detail_pizza;

CREATE TABLE public.order_detail_pizza
(
    order_detail_pizza_id serial NOT NULL,
    name character varying(255) COLLATE pg_catalog."default",
    price double precision,
    quantity integer,
    order_id integer NOT NULL,
    pizza_id integer NOT NULL,
    CONSTRAINT order_detail_pizza_pkey PRIMARY KEY (order_detail_pizza_id),
    CONSTRAINT fk7qu97gi0hjue7g4y9efl8jp5j FOREIGN KEY (order_id)
        REFERENCES public.orders (order_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fkjyfe4c05691y45p9c3dm5gd2y FOREIGN KEY (pizza_id)
        REFERENCES public.pizza (pizza_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.order_detail_pizza
    OWNER to postgres;
	
-- Table: public.order_detail_product

-- DROP TABLE public.order_detail_product;

CREATE TABLE public.order_detail_product
(
    order_detail_product_id serial NOT NULL,
    name character varying(255) COLLATE pg_catalog."default",
    price double precision,
    quantity integer,
    order_id integer NOT NULL,
    product_id integer NOT NULL,
    CONSTRAINT order_detail_product_pkey PRIMARY KEY (order_detail_product_id),
    CONSTRAINT fkdnh0yd9tlqwjtjbix63nrm74m FOREIGN KEY (order_id)
        REFERENCES public.orders (order_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fkge1cnef248fkp1d9r0apfodry FOREIGN KEY (product_id)
        REFERENCES public.product (product_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.order_detail_product
    OWNER to postgres;







