-- -- pizza size items
insert into pizza_size(size_id, size_description, slice_number, pizza_size_type, size_price) values(1, 'it is for you', 1, 'PERSONAL', 25);
insert into pizza_size(size_id, size_description, slice_number, pizza_size_type, size_price) values(2, 'it is ideal for love', 4, 'SMALL', 40);
insert into pizza_size(size_id, size_description, slice_number, pizza_size_type, size_price) values(3, 'it is ideal for family', 6, 'MEDIUM', 55);
insert into pizza_size(size_id, size_description, slice_number, pizza_size_type, size_price) values(4, 'it is ideal for friends', 8, 'LARGE', 70);
--
--
-- -- pizza type items
insert into pizza_type(pizza_type_id, pizza_type_name, pizza_type_description, pizza_type_price) values(1, 'Napoli', 'Queso, salchicha, carne, choclo, pimenton, cebolla.', 0);
insert into pizza_type(pizza_type_id, pizza_type_name, pizza_type_description, pizza_type_price) values(2, 'Hawaiana', 'Queso, jamon, piña, durazno.', 0);
insert into pizza_type(pizza_type_id, pizza_type_name, pizza_type_description, pizza_type_price) values(3, 'Calabresa', 'Queso, jamon, salame, hongos, pimenton.', 0);
insert into pizza_type(pizza_type_id, pizza_type_name, pizza_type_description, pizza_type_price) values(4, 'Italiana', 'Queso, tomate, jamon.', 0);
insert into pizza_type(pizza_type_id, pizza_type_name, pizza_type_description, pizza_type_price) values(5, 'Jamon', 'Queso, jamon.', 0);
insert into pizza_type(pizza_type_id, pizza_type_name, pizza_type_description, pizza_type_price) values(6, 'Fundida', 'Queso, jamon, salchichon, choclo, cebolla blanca.', 0);
insert into pizza_type(pizza_type_id, pizza_type_name, pizza_type_description, pizza_type_price) values(8, 'Pollo Choclo', 'Queso.', 0);
insert into pizza_type(pizza_type_id, pizza_type_name, pizza_type_description, pizza_type_price) values(9, 'Di Oliva', 'Queso, tomate, jamon, aceitunas negras-verdes, hongos.', 0);
insert into pizza_type(pizza_type_id, pizza_type_name, pizza_type_description, pizza_type_price) values(10, 'Criolla', 'Queso, carne, locoto, cebolla blanca, aceitunas negras.', 0);
insert into pizza_type(pizza_type_id, pizza_type_name, pizza_type_description, pizza_type_price) values(11, 'Carbonara', 'Queso, jamon, aceitunas verdes, pimenton.', 0);
insert into pizza_type(pizza_type_id, pizza_type_name, pizza_type_description, pizza_type_price) values(12, 'Vegetariana', 'Queso, hongos, tomate, palmito, pimenton, cebolla.', 0);
insert into pizza_type(pizza_type_id, pizza_type_name, pizza_type_description, pizza_type_price) values(13, 'Tocino', 'Queso, tocino, ciruelas, pasas de uvas desidratadas.', 5);
insert into pizza_type(pizza_type_id, pizza_type_name, pizza_type_description, pizza_type_price) values(14, 'Carnivora', 'Queso, carne, pollo, tocino, salame, aros de cebolla.', 5);
insert into pizza_type(pizza_type_id, pizza_type_name, pizza_type_description, pizza_type_price) values(15, 'Borde Relleno', 'Aceitunas verdes, aros de cebolla, oregano.', 5);
--
-- -- topping items
--
insert into topping(topping_id, topping_description, topping_price) values (1, 'Pepperoni', 5);
insert into topping(topping_id, topping_description, topping_price) values (2, 'Mushrooms', 5);
insert into topping(topping_id, topping_description, topping_price) values (3, 'Onions', 5);
insert into topping(topping_id, topping_description, topping_price) values (4, 'Sausage', 5);
insert into topping(topping_id, topping_description, topping_price) values (5, 'Bacon', 5);
insert into topping(topping_id, topping_description, topping_price) values (6, 'Black olives', 5);
insert into topping(topping_id, topping_description, topping_price) values (7, 'Green peppers', 5);
insert into topping(topping_id, topping_description, topping_price) values (8, 'Extra cheese', 5);
insert into topping(topping_id, topping_description, topping_price) values (9, 'Pineapple', 5);
insert into topping(topping_id, topping_description, topping_price) values (10, 'Spinach', 5);

--non pizza products
insert into product(product_id, product_description, product_name, product_price) values(1, 'Retornable', 'Coca-Cola 2 Litros', 13);
insert into product(product_id, product_description, product_name, product_price) values(2, 'Salvietti', 'Jugo de valle', 15);
insert into product(product_id, product_description, product_name, product_price) values(3, 'Mozzrella cheese, parmesan cheese, pasta sauce.', ' Spaghetti', 25);
insert into product(product_id, product_description, product_name, product_price) values(4, 'Black olives, pine nuts, olive oil', 'Salad', 10);
    

