-- USER
-- hashed password: letmein
INSERT INTO security_user (username, password, first_name, last_name) VALUES('admin', '$2a$12$ZhGS.zcWt1gnZ9xRNp7inOvo5hIT0ngN7N.pN939cShxKvaQYHnnu', 'Administrator', 'Adminstrator');
INSERT INTO security_user (username, password, first_name, last_name) VALUES('csr_jane', '$2a$12$ZhGS.zcWt1gnZ9xRNp7inOvo5hIT0ngN7N.pN939cShxKvaQYHnnu', 'Jane', 'Doe');
INSERT INTO security_user (username, password, first_name, last_name) VALUES('csr_mark', '$2a$12$ZhGS.zcWt1gnZ9xRNp7inOvo5hIT0ngN7N.pN939cShxKvaQYHnnu', 'Mark', 'Smith');
INSERT INTO security_user (username, password, first_name, last_name) VALUES('wally', '$2a$12$ZhGS.zcWt1gnZ9xRNp7inOvo5hIT0ngN7N.pN939cShxKvaQYHnnu', 'Walter', 'Adams');

-- ROLES

INSERT INTO security_role (role_name, description) VALUES ('ROLE_ADMIN', 'Administrator');
INSERT INTO security_role (role_name, description) VALUES ('ROLE_CSR', 'Customer Service Representative');
--
--
INSERT INTO user_role(user_id, role_id) VALUES (1, 1); -- give admin ROLE_ADMIN
INSERT INTO user_role(user_id, role_id) VALUES(2, 2);  -- give Jane ROLE_CSR
INSERT INTO user_role(user_id, role_id) VALUES(3, 2);  -- give Mark ROLE_CSR
INSERT INTO user_role(user_id, role_id) VALUES(4, 1);  -- give Wally ROLE_ADMIN
INSERT INTO user_role(user_id, role_id) VALUES(4, 2);  -- give Wally ROLE_CSR