# docker-restful-api

Docker Swarm Topology  example for restful java api. Example developed in windows 10 Environment

# Requirements to run the example 
## Create 1 manager and 2 workers
   For Windows 10 follow next reference : https://docs.docker.com/machine/drivers/hyper-v/
## Go manager node
   eval $("C:\Program Files\Docker\Docker\Resources\bin\docker-machine.exe" env <manager_name>)
## Deploy Docker Swarm
   prompt into git bash terminal into main folder where docker-compose.yml
   then run next command :
   docker stack deploy -c docker-compose.yml upDockerSw
   
## Test api endpoints
    Get IP Address from worker1 and worker2 "docker-machine ls"
    Look service info "docker service ps upDockerSw_api_pizza" and get what worker is running
    Accord to the worker and its IP Address go to postman or explorer with next url
    http://<manager-ip-address>/api/products  (get products catalog)
    http://<manager-ip-address>/api/toppings  (get toppings catalog)
    
    For api security get jwt token with next url
    http://<manager-ip-address>/users/signin   (get jwt token for admin user with payload {"username":"wally", "password":"letmein"})
    http://<manager-ip-address>/users/signup   (register new user, needs autorizathion jwt token for admin user)
   
        
   




